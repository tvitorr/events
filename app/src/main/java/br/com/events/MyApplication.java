package br.com.events;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import br.com.events.di.AppComponent;
import br.com.events.di.AppModule;
import br.com.events.di.DaggerAppComponent;

public class MyApplication extends Application {
    private static AppComponent component;
    private static Context context;


    public static SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences(getPackageName() + "_preferences", MODE_PRIVATE);
        context = this;
        if (component == null) {
            component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        }
    }

    public static AppComponent getAppComponent() {
        return component;
    }

    public static Context getContext() {
        return context;
    }

}
