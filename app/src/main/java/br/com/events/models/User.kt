package br.com.events.models


data class User(
    var email: String,
    var eventId: String,
    var name: String
) {}
