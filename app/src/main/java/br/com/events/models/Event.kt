package br.com.events.models

import java.util.*

data class Event(
    val date: Date,
    val description: String,
    val longitude: Double,
    val latitude: Double,
    val price: Double,
    val title: String,
    val id: String,
    val image: String
) {
}