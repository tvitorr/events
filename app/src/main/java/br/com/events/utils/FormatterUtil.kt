package br.com.events.utils

import android.location.Address
import java.text.DateFormatSymbols
import java.util.*

class FormatterUtil {

    companion object {

        fun formatDate(s: String): String {
            val splittedDate = s.split("T")[0]
            val day = splittedDate.split(" ")[2]
            val month =
                splittedDate.split(" ")[1]
            return "$day/$month"
        }

        fun formatFullAddress(local: Address): String {
            return (local.thoroughfare + ", " + local.subThoroughfare + ", " + local.subLocality + "\n" + local.subAdminArea + ", " +
                    local.adminArea + " - " + local.countryCode)
        }
        fun formatAddress(local: Address): String {
            return ( local.subAdminArea + ", " + local.adminArea )
        }

        fun formatCurrency(value: Double?): String = String.format(Locale("pt", "BR"), "R$ %,.2f", value)

    }


}