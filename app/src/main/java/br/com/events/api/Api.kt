package br.com.events.api

import br.com.events.models.Event
import br.com.events.models.User
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface Api {

    @GET("events")
    fun getEventList(
    ): Observable<List<Event>>

    @GET("events/{id}")
    fun getEventDetail(
    @Path("id") id:String): Observable<Event>

    @POST("checkin")
     fun postCheckin(
        @Body user: User?
    ): Observable<ResponseBody>


    companion object {
        internal var BASE_URL: String = "https://5b840ba5db24a100142dcd8c.mockapi.io/api/"
    }
}