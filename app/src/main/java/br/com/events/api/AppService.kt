package br.com.events.api

import android.content.Context
import br.com.events.models.Event
import br.com.events.models.User
import io.reactivex.Observable
import okhttp3.ResponseBody
import javax.inject.Singleton

@Singleton
class AppService(private val api: Api, private val context: Context) {
    fun doRequestEvents(): Observable<List<Event>> {
        return api.getEventList()
    }

    fun doRequestEventDetails(id: String): Observable<Event> {
        return api.getEventDetail(id)
    }

    fun doRequestCheckin(user: User): Observable<ResponseBody> {
        return api.postCheckin(user)
    }


}