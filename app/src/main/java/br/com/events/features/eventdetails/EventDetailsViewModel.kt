package br.com.events.features.eventdetails

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import br.com.events.models.Event
import br.com.events.models.User
import okhttp3.ResponseBody

class EventDetailsViewModel(private var model: EventDetailsMVVM.DomainModel) :
    EventDetailsMVVM.ViewModel {

    private lateinit var view: EventDetailsMVVM.View

    private var progress: ObservableBoolean = ObservableBoolean()
    private var event: ObservableField<Event> = ObservableField()


    override fun setView(view: EventDetailsMVVM.View) {
        this.view = view
    }

    override fun onCreate(id: String) {
        view.setViewModelVariable()
        progress.set(true)
        model.setViewModel(this)
        model.requestEvents(id)
    }

    override fun onRequestEventsSuccess(eventDetail: Event) {
        event.set(eventDetail)
        view.setAllVariables(eventDetail)
        progress.set(false)
    }

    override fun onRequestCheckinSuccess(response: ResponseBody) {
        progress.set(false)
        view.notifyCheckinSuccess()
    }

    override fun onRequestError(message: String?) {
        progress.set(false)
        view.notifyShowError(message)
    }

    override fun onCheckinError(message: String?) {
        progress.set(false)
        view.notifyCheckinError(message)
    }

    override fun onClickShareEvent() {
        view.shareEvent(event.get()!!)
    }

    override fun onclickCheckinEvent() {
        progress.set(true)
        var user = User("email@email.com", event.get()!!.id, "nome")
        model.requestCheckin(user)
    }

    override fun isProgressEvent(): ObservableBoolean {
        return progress
    }

    override fun getEvent(): ObservableField<Event> {
        return event
    }
}