package br.com.events.features.eventdetails

import br.com.events.api.AppService
import br.com.events.models.Event
import br.com.events.models.User
import br.com.events.utils.ObserverUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody

class EventDetailsDomainModel(private val appService: AppService) :
    EventDetailsMVVM.DomainModel {

    private lateinit var viewModel: EventDetailsMVVM.ViewModel

    override fun setViewModel(viewModel: EventDetailsMVVM.ViewModel) {
        this.viewModel = viewModel
    }

    override fun requestEvents(id: String) {
        appService.doRequestEventDetails(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ObserverUtils<Event>() {
                override fun onComplete() {
                    viewModel.onRequestEventsSuccess(genericVariable!!)
                }

                override fun onError(e: Throwable) {
                    viewModel.onRequestError(e.message)
                }
            })
    }

    override fun requestCheckin(user: User) {
        appService.doRequestCheckin(user)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ObserverUtils<ResponseBody>() {
                override fun onComplete() {
                    viewModel.onRequestCheckinSuccess(genericVariable!!)
                }

                override fun onError(e: Throwable) {
                    viewModel.onCheckinError(e.message)
                }
            })
    }

}