package br.com.events.features.eventlist

import br.com.events.api.AppService
import br.com.events.models.Event
import br.com.events.utils.ObserverUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EventsDomainModel(private var appService: AppService) :
    EventsMVVM.DomainModel {

    private lateinit var viewModel: EventsMVVM.ViewModel

    override fun setViewModel(viewModel: EventsMVVM.ViewModel) {
        this.viewModel = viewModel
    }

    override fun requesEvents() {
        appService.doRequestEvents()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : ObserverUtils<List<Event>>() {
                override fun onComplete() {
                    viewModel.onRequestEventsSuccess(genericVariable)
                }

                override fun onError(e: Throwable) {
                    viewModel.onRequestError(e.message)
                }
            })
    }

}