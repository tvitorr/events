package br.com.events.features.eventdetails

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import br.com.events.models.Event
import br.com.events.models.User
import okhttp3.ResponseBody

interface EventDetailsMVVM {
    interface View {
        fun setViewModelVariable()
        fun setAllVariables(events: Event?)
        fun notifyCheckinSuccess()
        fun notifyCheckinError(message:String?)
        fun notifyShowError(message: String?)
        fun shareEvent(event: Event)
    }

    interface ViewModel {
        fun setView(view: View)
        fun onCreate(id: String)
        fun onRequestEventsSuccess(events: Event)
        fun onRequestCheckinSuccess(response: ResponseBody)
        fun onRequestError(message: String?)
        fun onCheckinError(message:String?)
        fun onClickShareEvent()
        fun onclickCheckinEvent()
        fun isProgressEvent(): ObservableBoolean
        fun getEvent(): ObservableField<Event>
    }

    interface DomainModel {
        fun setViewModel(viewModel: ViewModel)
        fun requestEvents(id: String)
        fun requestCheckin(user: User)
    }
}