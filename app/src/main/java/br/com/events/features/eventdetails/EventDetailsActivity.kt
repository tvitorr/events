package br.com.events.features.eventdetails

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.events.MyApplication
import br.com.events.R
import br.com.events.databinding.ActivityEventDetailsBinding
import br.com.events.models.Event
import br.com.events.utils.FormatterUtil
import com.bumptech.glide.Glide
import javax.inject.Inject

class EventDetailsActivity : AppCompatActivity(),
    EventDetailsMVVM.View {

    @Inject
    lateinit var viewModel: EventDetailsMVVM.ViewModel

    lateinit var binding: ActivityEventDetailsBinding

    lateinit var geocoder: Geocoder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApplication.getAppComponent().inject(this)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_event_details
        )
        viewModel.setView(this)
        viewModel.onCreate(getEventId())
        geocoder = Geocoder(this)
    }

    override fun setViewModelVariable() {
        binding.viewModel = viewModel
    }


    override fun setAllVariables(events: Event?) {
        binding.eventDetailsImageView.visibility = View.VISIBLE
        binding.constraintLayout.visibility = View.VISIBLE
        bindingEvenDetails(events)
    }

    override fun notifyCheckinSuccess() {
        showCheckinDialog("Sua presença foi confirmada com sucesso!")
    }

    override fun notifyCheckinError(message: String?) {
        showCheckinDialog("Algo de errado aconteceu!\nSua presença não foi confirmada.")
    }

    override fun notifyShowError(message: String?) {
        binding.eventDetailsActivityError.containerError.visibility = View.VISIBLE
        binding.eventDetailsActivityError.errorLayoutButton.setOnClickListener {
            viewModel.onCreate(getEventId())
            binding.eventDetailsActivityError.containerError.visibility = View.GONE
        }
    }

    override fun shareEvent(event: Event) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, event.title + "\n" + event.description)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    private fun bindingEvenDetails(event: Event?) {
        val local = geocoder.getFromLocation(event?.latitude!!, event.longitude, 1)[0]
        binding.eventDetailsActivityDescription.text = event.description
        binding.eventDetailsActivityTitle.text = event.title
        binding.eventDetailsActivityLocal.text = (FormatterUtil.formatFullAddress(local))
        binding.eventDetailsDate.text = FormatterUtil.formatDate(event.date.toString())
        binding.eventDetailsPrice.text = FormatterUtil.formatCurrency(event.price)
        Glide.with(this).load(event.image).circleCrop()
            .into(binding.eventDetailsImageView)
    }

    private fun getEventId(): String {
        val bundle = this.intent.extras!!
        return bundle.getSerializable("eventId") as String
    }


    private fun showCheckinDialog(message: String) {
        val dialog = AlertDialog.Builder(this)
            .setTitle("Confirmação de presença")
            .setMessage(message)
            .setPositiveButton("Voltar") { _, _ ->
                finish()
            }
            .create()
        dialog.show()
    }
}
