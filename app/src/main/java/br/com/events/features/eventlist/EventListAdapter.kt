package br.com.events.features.eventlist

import android.app.Activity
import android.location.Geocoder
import br.com.events.R
import br.com.events.common.adapter.BindingViewHolder
import br.com.events.common.adapter.DataBindingAdapter
import br.com.events.databinding.EventItemBinding
import br.com.events.models.Event
import br.com.events.utils.FormatterUtil
import com.bumptech.glide.Glide

class EventListAdapter(
    private val activity: Activity?,
    private val viewModel: EventsMVVM.ViewModel
) : DataBindingAdapter<EventItemBinding?, Event?>(activity!!) {
    override val itemResource: Int
        get() = R.layout.event_item

    override fun onBindViewHolder(holder: BindingViewHolder<EventItemBinding?>, position: Int) {

        val item = getItems()[position]
        val geoCoder = Geocoder(activity)
        val local = geoCoder.getFromLocation(item?.latitude!!, item.longitude, 1)[0]


        holder.binding?.eventItemTitle?.text = item.title
        holder.binding?.eventItemSubTitle?.text = FormatterUtil.formatAddress(local)
        holder.binding?.eventItemDescription?.text = item.description
        holder.binding?.eventItemContainer?.setOnClickListener { viewModel.onItemClicked(item) }
        holder.binding?.eventItemDate?.text =
            FormatterUtil.formatDate(item.date.toString())

        activity?.let {
            Glide.with(it).load(item.image).circleCrop()
                .into(holder.binding!!.eventItemImageView)
        }
    }
}