package br.com.events.features.eventlist

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import br.com.events.models.Event

class EventsViewModel(private var model: EventsMVVM.DomainModel) : EventsMVVM.ViewModel {

    private lateinit var view: EventsMVVM.View
    private var progress: ObservableBoolean = ObservableBoolean()
    private var selectedEvent: ObservableField<Event> = ObservableField()

    override fun setView(view: EventsMVVM.View) {
        this.view = view
    }

    override fun onCreate() {
        model.setViewModel(this)
        progress.set(true)
        model.requesEvents()
        view.setAllVariables()
    }

    override fun onItemClicked(item: Event?) {
        selectedEvent.set(item)
        view.notifyItemClicked(item!!.id)
    }

    override fun onRequestEventsSuccess(events: List<Event>?) {
        progress.set(false)
        view.setEventsAdapter(events)
    }

    override fun onRequestError(message: String?) {
        progress.set(false)
        view.notifyShowError(message)
    }

    override fun isProgressEvent(): ObservableBoolean {
        return progress
    }
}