package br.com.events.features.eventlist

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.events.MyApplication
import br.com.events.R
import br.com.events.databinding.ActivityEventsBinding
import br.com.events.features.eventdetails.EventDetailsActivity
import br.com.events.models.Event
import javax.inject.Inject

class EventsActivity : AppCompatActivity(),
    EventsMVVM.View {

    @Inject
    lateinit var viewModel: EventsMVVM.ViewModel

    lateinit var adapter: EventListAdapter

    lateinit var binding: ActivityEventsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MyApplication.getAppComponent().inject(this)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_events
        )
        viewModel.setView(this)
        viewModel.onCreate()
    }

    override fun setAllVariables() {
        binding.viewModel = viewModel
    }

    override fun setEventsAdapter(events: List<Event>?) {
        adapter =
            EventListAdapter(this, viewModel)
        binding.eventsActivityRecyler.layoutManager = LinearLayoutManager(this)
        binding.eventsActivityRecyler.adapter = adapter
        adapter.setItems(events)
    }

    override fun notifyShowError(message: String?) {
        binding.eventsActivityError.containerError.visibility = View.VISIBLE
        binding.eventsActivityError.errorLayoutButton.setOnClickListener {
            viewModel.onCreate()
            binding.eventsActivityError.containerError.visibility = View.GONE
        }
    }

    override fun notifyItemClicked(id: String) {
        val intent =
            startActivity(
                Intent(this, EventDetailsActivity::class.java).putExtra("eventId", id)
            )
    }
}
