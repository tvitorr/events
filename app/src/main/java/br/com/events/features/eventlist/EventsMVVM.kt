package br.com.events.features.eventlist

import androidx.databinding.ObservableBoolean
import br.com.events.models.Event

interface EventsMVVM {

    interface View {
        fun setAllVariables()
        fun setEventsAdapter(events: List<Event>?)
        fun notifyShowError(message: String?)
        fun notifyItemClicked(id:String)
    }

    interface ViewModel {
        fun setView(view: View)
        fun onCreate()
        fun onItemClicked(item: Event?)
        fun onRequestEventsSuccess(events: List<Event>?)
        fun onRequestError(message: String?)
        fun isProgressEvent(): ObservableBoolean
    }

    interface DomainModel {
        fun setViewModel(viewModel: ViewModel)
        fun requesEvents()
    }
}