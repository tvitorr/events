package br.com.events.di

import br.com.events.features.eventdetails.EventDetailsActivity
import br.com.events.features.eventlist.EventsActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, MVVMModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(activity: EventsActivity)

    fun inject(activity: EventDetailsActivity)
}