package br.com.events.di

import br.com.events.features.eventlist.EventsDomainModel
import br.com.events.features.eventlist.EventsMVVM
import br.com.events.features.eventlist.EventsViewModel
import br.com.events.api.AppService
import br.com.events.features.eventdetails.EventDetailsDomainModel
import br.com.events.features.eventdetails.EventDetailsMVVM
import br.com.events.features.eventdetails.EventDetailsViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MVVMModule {

    @Provides
    @Singleton
    fun provideEventsViewModel(model: EventsMVVM.DomainModel): EventsMVVM.ViewModel {
        return EventsViewModel(model)
    }

    @Provides
    @Singleton
    fun provideEventsDomainModel(appService: AppService): EventsMVVM.DomainModel {
        return EventsDomainModel(appService)
    }

    @Provides
    @Singleton
    fun provideEventDetailsViewModel(model: EventDetailsMVVM.DomainModel): EventDetailsMVVM.ViewModel {
        return EventDetailsViewModel(model)
    }

    @Provides
    @Singleton
    fun provideEventDetailsDomainModel(appService: AppService): EventDetailsMVVM.DomainModel {
        return EventDetailsDomainModel(appService)
    }

}