# Teste para Desenvolvedor(a) Android - Sicredi

Nessa aplicação o usuário pode:

- Ver sua lista de eventos;
- Ver detalhes dos eventos;
- Confirmar presença em eventos;
- Compartilhar eventos.

# Requisitos mínimos 
- [x]App deve compilar sem a necessidade de nenhum ajuste após ser clonado
- [x]Suporte à API 19 e funcionar com a API 29
- [x]Código deve ser escrito em Kotlin

# Layout
Protótipo criado usando a ferramenta Figma disponível neste link:
`https://bit.ly/2KISqYm`